require 'rspec'
require './filter_input'

describe 'FilterInput' do
  it 'should equal { :year => 2018, :make => "Ford", :model => "Focus", :trim => nil }' do
    input = { :year => '2018', :make => 'fo', :model => 'focus', :trim => 'blank' }
    output = { :year => 2018, :make => 'Ford', :model => 'Focus', :trim => nil }
    filtered_input = FilterInput.new(input)

    expect(filtered_input.normalize_data).to eq(output)
  end

  it 'should equal { :year => "200", :make => "blah", :model => "foo", :trim => "bar" }' do
    input = { :year => '200', :make => 'blah', :model => 'foo', :trim => 'bar' }
    output = { :year => '200', :make => 'blah', :model => 'foo', :trim => 'bar' }
    filtered_input = FilterInput.new(input)

    expect(filtered_input.normalize_data).to eq(output)
  end

  it 'should equal { :year => 1999, :make => "Chevrolet", :model => "Impala", :trim => "ST" }' do
    input = { :year => '1999', :make => 'Chev', :model => 'IMPALA', :trim => 'st' }
    output = { :year => 1999, :make => 'Chevrolet', :model => 'Impala', :trim => 'ST' }
    filtered_input = FilterInput.new(input)

    expect(filtered_input.normalize_data).to eq(output)
  end

  it 'should equal { :year => 2000, :make => "Ford", :model => "Focus", :trim => "SE" }' do
    input = { :year => '2000', :make => 'ford', :model => 'focus se', :trim => '' }
    output = { :year => 2000, :make => 'Ford', :model => 'Focus', :trim => 'SE' }
    filtered_input = FilterInput.new(input)

    expect(filtered_input.normalize_data).to eq(output)
  end

  it 'should equal {:year => "", :make => "", :model => "", :trim => "" }' do
    input = {:year => "", :make => "", :model => "", :trim => "" }
    filtered_input = FilterInput.new(input)

    expect(filtered_input.normalize_data).to eq(input)
  end

  it 'should equal { :year => 2005, :make => "Chevrolet", :model => "Cobalt", :trim => "LS" }' do
    input = { :year => '2005', :make => 'Chevrolet', :model => 'Cobalt LS', :trim => '' }
    output = { :year => 2005, :make => 'Chevrolet', :model => 'Cobalt', :trim => 'LS' }
    filtered_input = FilterInput.new(input)

    expect(filtered_input.normalize_data).to eq(output)
  end
end
