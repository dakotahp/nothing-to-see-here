# Ruby 2.5.0

class FilterInput
  attr_reader :make, :model, :trim, :year

  KNOWN_MAKES = [
    'Ford',
    'Chevrolet',
  ]

  KNOWN_MODELS = [
    'Cobalt',
    'Focus',
    'Impala',
  ]

  KNOWN_TRIMS = [
    'LS',
    'SE',
    'ST',
  ]

  def initialize(input)
    @year = remove_blanks(input.dig(:year))
    @make = remove_blanks(input.dig(:make))
    @model = remove_blanks(input.dig(:model))
    @trim = remove_blanks(input.dig(:trim))
  end

  def normalize_data

    {
      year: normalized_year,
      make: normalized_make,
      model: normalized_model,
      trim: normalized_trim,
    }
  end

  private

  def remove_blanks(input)
    if input == "blank"
      nil
    else
      input
    end
  end

  def normalized_year
    valid_range = 1900..(Time.new.year + 2)
    if valid_range.include?(year.to_i)
      year.to_i
    else
      year
    end
  end

  def normalized_make
    return make if make.empty?

    KNOWN_MAKES.each do |known_make|
      if known_make.match(/#{make}/i)
        return known_make
      end
    end

    make
  end

  def normalized_model
    return model if model.empty?

    KNOWN_MODELS.each do |known_model|
      if model.match(/#{known_model}/i)
        return known_model
      end
    end

    model
  end

  def normalized_trim
    return nil if trim.nil?
    return trim if trim.empty? && normalized_model.empty?

    KNOWN_TRIMS.each do |known_trim|
      if !trim.empty? && known_trim.match(/#{trim}/i)
        return known_trim
      elsif trim.empty? && known_trim.match(/#{model.split(" ").last}/i)
        return known_trim
      end
    end

    trim
  end
end

examples = [
  [{ :year => '2018', :make => 'fo', :model => 'focus', :trim => 'blank' },
   { :year => 2018, :make => 'Ford', :model => 'Focus', :trim => nil }],
  [{ :year => '200', :make => 'blah', :model => 'foo', :trim => 'bar' },
   { :year => '200', :make => 'blah', :model => 'foo', :trim => 'bar' }],
  [{ :year => '1999', :make => 'Chev', :model => 'IMPALA', :trim => 'st' },
   { :year => 1999, :make => 'Chevrolet', :model => 'Impala', :trim => 'ST' }],
  [{ :year => '2000', :make => 'ford', :model => 'focus se', :trim => '' },
   { :year => 2000, :make => 'Ford', :model => 'Focus', :trim => 'SE' }],
  [{:year => '', :make => '', :model => '', :trim => '' },
   { :year => '', :make => '', :model => '', :trim => '' }],
  [{ :year => '2005', :make => 'Chevrolet', :model => 'Cobalt LS', :trim => '' },
  { :year => 2005, :make => 'Chevrolet', :model => 'Cobalt', :trim => 'LS' }],
]

examples.each_with_index do |(input, expected_output), index|
  filtered_input = FilterInput.new(input)
  if (output = filtered_input.normalize_data) == expected_output
    puts "Example #{index + 1} passed!"
  else
    puts "Example #{index + 1} failed,
          Expected: #{expected_output.inspect}
          Got:      #{output.inspect}"
  end
end
